/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
	说明:
		该示例主要是演示创建两个不同优先级的任务。
		注意不同优先级与相同优先级任务的调度执行情况
		把这里的打印输出与示例2的打印输出做比较。
		结合任务调度与延迟一起来思考，为什么会是这样的执行情况？
		然后我们可以运行一下例4，再把示例2，3，4代码与现象一起比较
*/

/* FreeRTOS.org 源文件头包含 */
#include "FreeRTOS.h"
#include "task.h"

/* 示例包含头文件 */
#include "supporting_functions.h"

/* 使用循环计数来实现延时功能 */
#define mainDELAY_LOOP_COUNT		( 0xffffff )

/* 任务函数声明 */
void vTaskFunction( void *pvParameters );

/* 定义字符串，将做为参数传递给任务函数.
这些值定义为常量，并且当任务正在执行时，
确保离开栈区，它们指定的值仍然有效。 */
const char *pcTextForTask1 = "Task 1 is running\r\n";
const char *pcTextForTask2 = "Task 2 is running\r\n";

/*-----------------------------------------------------------*/

int main( void )
{
	/* 创建一个优先级为 1 的任务 */
	xTaskCreate( vTaskFunction, "Task 1", 1000, (void*)pcTextForTask1, 1, NULL );

	/* 创建第二个任务，优先级为2,参数为倒数第二个参数 */
	xTaskCreate( vTaskFunction, "Task 2", 1000, (void*)pcTextForTask2, 2, NULL );

	/* 开启调度，使得任务被执行 */
	vTaskStartScheduler();	

	/* 下面的代码应该是永远不会被执行的，除非vTaskStartScheduler() 函数没有足够的堆(heap)
	空间创建空闲任务和定时器(如果配置了定时器)任务。关于堆管理和捕获堆空间耗尽的相关技术，
	在书本正文中有描述。这里的堆就是我们常说的内存，大多数情况下是指高速访问内存RAM.*/
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

void vTaskFunction( void *pvParameters )
{
char *pcTaskName;
volatile uint32_t ul;

	/* 将要被打印的字符串是通过参数传递过来的.  把它强制转换为字符串指针类型. */
	pcTaskName = ( char * ) pvParameters;

	/* 跟大多数任务一样，这个任务的实现是一个无限循环 */
	for( ;; )
	{
		/* 打印本任务的名字 */
		vPrintString( pcTaskName );

		/* 延迟一段时间 */
		for( ul = 0; ul < mainDELAY_LOOP_COUNT; ul++ )
		{
			/* 这个循环是一种暴力(不精确的)延时.  这里什么也不做，
			后面的代码我们会使用delay或sleep 函数来替换这个循环 */
		}
	}
}


