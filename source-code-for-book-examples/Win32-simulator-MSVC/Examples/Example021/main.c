/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
	说明:
		
*/

/* 标准头文件包含 */
#include <stdio.h>
#include <conio.h>

/* FreeRTOS.org 源文件头包含 */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* 示例包含 */
#include "supporting_functions.h"

/* 这个任务是把消息发送给stdio gatekeeper.创建两个该任务的实例. */
static void prvPrintTask( void *pvParameters );

/*  gatekeeper 任务. */
static void prvStdioGatekeeperTask( void *pvParameters );

/* 定义任务和中断将通过gatekeeper打印输出的字符串 */
static const char *pcStringsToPrint[] =
{
	"Task 1 ****************************************************\r\n",
	"Task 2 ----------------------------------------------------\r\n",
	"Message printed from the tick hook interrupt ##############\r\n"
};

/*-----------------------------------------------------------*/

/* 声明QueueHandle_t类型变量,我们利用这个变量把消息发送到gatekeeper.*/
static QueueHandle_t xPrintQueue;

/* 任务将阻塞在 0到xMaxBlockTime 之间的一段伪随机时间. */
const TickType_t xMaxBlockTimeTicks = 0x20;

int main( void )
{
    /* queue在使用之前必须创建,这个队列最多可以持有5个字符指针 */
    xPrintQueue = xQueueCreate( 5, sizeof( char * ) );

	/* 检查队列是否成功创建. */
	if( xPrintQueue != NULL )
	{
		/* 创建两个任务实例用来给gatekeeper发送消息。
		这个消息就是字符串数组的索引，这个索引是创建时传递给任务的参数( xTaskCreate() 的第4个参数)
		并且任务的优先级是不相同的，这样就会有任务抢占调度的发生。 */
		xTaskCreate( prvPrintTask, "Print1", 1000, ( void * ) 0, 1, NULL );
		xTaskCreate( prvPrintTask, "Print2", 1000, ( void * ) 1, 2, NULL );

		/* 创建一个gatekeeper任务。只有这个任务可以访问标准输出。 */
		xTaskCreate( prvStdioGatekeeperTask, "Gatekeeper", 1000, NULL, 0, NULL );

		/* 开启调度，使任务可以执行 */
		vTaskStartScheduler();
	}

	/* 下面的代码应该是永远不会被执行的，除非vTaskStartScheduler() 函数没有足够的堆(heap)
	空间创建空闲任务和定时器(如果配置了定时器)任务。关于堆管理和捕获堆空间耗尽的相关技术，
	在书本正文中有描述。这里的堆就是我们常说的内存，大多数情况下是指高速访问内存RAM. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

static void prvStdioGatekeeperTask( void *pvParameters )
{
char *pcMessageToPrint;

	/* 这是唯一可以向终端写入数据的任务。其他想要输出信息的任务，不用直接访问输出终端，
	而是把输入信息发给本任务。
	由于只有本任务向终端输出信息，所以就不用考虑互斥和数据串扰问题。 */
	for( ;; )
	{
		/* 等待一个消息到达. */
		xQueueReceive( xPrintQueue, &pcMessageToPrint, portMAX_DELAY );

		/* 由于任务会一直阻塞，直到有消息到来，因此我们不需要检查它的返回值。
		当下一行被执行，将会有消息输出 */
		printf( "%s", pcMessageToPrint );
		fflush( stdout );

		/* 接下来，只需要继续循环等待下一条消息的到来. */
	}
}
/*-----------------------------------------------------------*/

void vApplicationTickHook( void )
{
static int iCount = 0;

	/* 每200个ticks发送一条消息.这个消息不会直接输出，而是发送给
	gatekeeper任务 */
	iCount++;
	if( iCount >= 200 )
	{
		/* 这个例子的最后一个参数xHigherPriorityTaskWoken,实际没有用，所以设置为NULL*/
		xQueueSendToFrontFromISR( xPrintQueue, &( pcStringsToPrint[ 2 ] ), NULL );

		/* 复位计数器值 */
		iCount = 0;
	}
}
/*-----------------------------------------------------------*/

static void prvPrintTask( void *pvParameters )
{
int iIndexToString;

	/* 该任务有创建两个实例，所以发送给gatekeeper任务的字符串索引，是从task
	的 parameter中传入进来的。进行类型转换 */
	iIndexToString = ( int ) pvParameters;

	for( ;; )
	{
		/* 输出字符串，但不直接输出，而是把消息通过消息队列发送到gatekeeper.
		队列在任务调度之前就已创建了，所以本任务执行时已经存在。
		这里没有指定阻塞时间，是因为队列应该一直是空的。*/
		xQueueSendToBack( xPrintQueue, &( pcStringsToPrint[ iIndexToString ] ), 0 );

		/* 等待一个伪随机时间。注意这个rand()不一定是可重入的，
		但是在这个例子中，代码并不关心它的返回值，所以rand()是否可重入
		无关紧要。在更多严格的应用中，要用可以重入rand()版本，或者在调用
		rand()时，要对其进行临界保护。 */
		vTaskDelay( ( rand() % xMaxBlockTimeTicks ) );
	}
}
/*-----------------------------------------------------------*/


/* 在其他例子中，该函数是在supporting_functions.c 源码中实现的.
但是本例不包括supporting_functions.c这个源码，所以如果包含了supporting_functions.c
源码，那么会导致tick钩子函数重复定义. */
void vAssertCalled( uint32_t ulLine, const char * const pcFile )
{
/* 接下来的两个变量，是为了不被编译器优化，因此在调试中该两变量不可见. */
volatile uint32_t ulLineNumber = ulLine, ulSetNonZeroInDebuggerToReturn = 0;
volatile const char * const pcFileName = pcFile;

	taskENTER_CRITICAL();
	{
		while( ulSetNonZeroInDebuggerToReturn == 0 )
		{
			/* 如果要在调试器中设置此函数以查看assert（）位置，
			请将ulSetNonZeroInDebuggerToReturn设置为非零值。 */
		}
	}
	taskEXIT_CRITICAL();

	/* 下面的代码纯粹是为了去除可能的编译警告，因为这两个变量只声明了没有引用。 */
	( void ) pcFileName;
	( void ) ulLineNumber;
}
/*-----------------------------------------------------------*/

/* 在其他例子中，该函数是在supporting_functions.c 源码中实现的.
但是本例不包括supporting_functions.c这个源码，所以如果包含了supporting_functions.c
源码，那么会导致vApplicationMallocFailedHook钩子函数重复定义. */
void vApplicationMallocFailedHook( void )
{
	/* vApplicationMallocFailedHook() 只有当在配置文件FreeRTOSConfig.h中
	将configUSE_MALLOC_FAILED_HOOK设置为1时，才会生效。
	当调用pvPortMalloc()失败时，这个钩子函数就会被调用。
	pvPortMalloc()函数，在创建任务，队列，定时器，事件组，或信号量创建时，
	被内核自动调用。pvPortMalloc()函数还会在应用中被调用。
	如果heap_1.c，heap_2.c或者heap_4.c被使用，那么pvPortMalloc()的可用堆空间
	大小由 FreeRTOSConfig.h中的configTOTAL_HEAP_SIZE决定的，并且我们可以用
	xPortGetFreeHeapSize() 获取堆空间还剩多少。更多信息在书中有描述 */
	vAssertCalled( __LINE__, __FILE__ );
}
/*-----------------------------------------------------------*/



