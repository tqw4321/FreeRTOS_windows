/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
	说明:
		本示例主要演示了不同优先级的任务，通过互斥量来访问临界资源
		本例的临界资源是stdout.

		源于热爱，乐于分享
*/

/* 标准头文件包含 */
#include <stdio.h>
#include <conio.h>

/* FreeRTOS.org 源文件头包含 */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* 示例包含头文件 */
#include "supporting_functions.h"

/* 要创建的任务.  该任务创建两个实例. */
static void prvPrintTask( void *pvParameters );

/* 本函数使用mutex互斥量，控制访问输出. */
static void prvNewPrintString( const char *pcString );

/*-----------------------------------------------------------*/

/* 声明一个SemaphoreHandle_t类型的变量,
用于引用互斥变量，以确保对stdout的互斥访问 */
SemaphoreHandle_t xMutex;

/* 任务将阻塞在 0到xMaxBlockTime 之间的一段伪随机时间 */
const TickType_t xMaxBlockTimeTicks = 0x20;

int main( void )
{
    /* 在使用信号量这前先要创建，
	本例创建一个mutex类型信号量
	(注意创建函数，
	在示例16用的xSemaphoreCreateBinary，(二值信号量)
	示例17是 xSemaphoreCreateCounting，（记数信号量）
	本示例是 xSemaphoreCreateMutex （互斥信号量）
	) */
    xMutex = xSemaphoreCreateMutex();

	/* 检查信号量是否成功创建. */
	if( xMutex != NULL )
	{
		/* 创建两个向stdout发送数据的实例，发送的字符串都是
		传递给任务的参数。这两个任务的优先级不同，这样就会有抢占发生. */
		xTaskCreate( prvPrintTask, "Print1", 1000, "Task 1 ******************************************\r\n", 1, NULL );
		xTaskCreate( prvPrintTask, "Print2", 1000, "Task 2 ------------------------------------------\r\n", 2, NULL );

		/* 开启调度，执行任务 */
		vTaskStartScheduler();
	}

	/* 下面的代码应该是永远不会被执行的，除非vTaskStartScheduler() 函数没有足够的堆(heap)
	空间创建空闲任务和定时器(如果配置了定时器)任务。关于堆管理和捕获堆空间耗尽的相关技术，
	在书本正文中有描述。这里的堆就是我们常说的内存，大多数情况下是指高速访问内存RAM. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

static void prvNewPrintString( const char *pcString )
{
	/* 该信号量在任务开启调度之前就已经创建，所以本任务执行的时候
	该信号量已经存在.

	尝试获取信号量，如果mutext还不可用，就一直阻塞。
	xSemaphoreTake()函数只有获取到信号量的值才会返回，所以就不需要检查返回值了.
	如果使用了其他延迟，那么就需要在访问其他资源(本例中的资源就是标准输出)之前检查xSemaphoreTake()的返回
	值是否为pdTRUE. */
	xSemaphoreTake( xMutex, portMAX_DELAY );
	{
		/* 只有成功获取信号量，下面的代码才会被执行--这样就可以任意的进行标准输出了 */
		printf( "%s", pcString );
		fflush( stdout );
	}
	xSemaphoreGive( xMutex );

	/* 允许任保按键停止应用运行。在真实的应用玩境，对按键的访问也应要受到保护的.
	在真实的应用中一般也不会有一个以上的任务处理按键值 */
	if( _kbhit() != 0 )
	{
		vTaskEndScheduler();
	}
}
/*-----------------------------------------------------------*/

static void prvPrintTask( void *pvParameters )
{
char *pcStringToPrint;
const TickType_t xSlowDownDelay = pdMS_TO_TICKS( 5UL );

	/* 该任务的两个实例已创建好了。任务打印的字符串，是由任务的参数传入进来的。
	这个参数需要转换成相应要求的类型。 */
	pcStringToPrint = ( char * ) pvParameters;

	for( ;; )
	{
		/* 使用新定义的函数打印输出字符串 */
		prvNewPrintString( pcStringToPrint );

		/* 等待一个伪随机时间。注意这个rand()不一定是可重入的，
		但是在这个例子中，代码并不关心它的返回值，所以rand()是否可重入
		无关紧要。在更多严格的应用中，要用可以重入rand()版本，或者在调用
		rand()时，要对其进行临界保护. */
		vTaskDelay( rand() % xMaxBlockTimeTicks );

		/* 加一个延迟，让显示滚动不要太快! */
		vTaskDelay( xSlowDownDelay );
	}
}



