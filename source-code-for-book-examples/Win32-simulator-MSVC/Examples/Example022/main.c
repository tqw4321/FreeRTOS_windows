/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

	说明:
		本示例演示了事件组的使用。
		通过对事件组的置位和清零，来在各个任务之前传递消息.
		事件组标志位的总位数，即是TickType_t类型的位数.
		事件组丰富了任务之间的消息传递方式。
*/

/* FreeRTOS.org 源文件头包含 */
#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"
#include "timers.h" /* 为了使用 xTimerPendFunctionCallFromISR() 函数. */

/* 示例包含 */
#include "supporting_functions.h"

/* 本例中使用的模拟中断号。0到2中断号已由FreeRTOS系统自身使用.所以3是第一个应用
可以使用的中断号 */
#define mainINTERRUPT_NUMBER	3

/* 定事件组中的事件位. */
#define mainFIRST_TASK_BIT	( 1UL << 0UL ) /* 事件 bit 0, 由任务设置. */
#define mainSECOND_TASK_BIT	( 1UL << 1UL ) /* 事件 bit 1, 由任务设置. */
#define mainISR_BIT			( 1UL << 2UL ) /* 事件 bit 2, 由 ISR设置. */

/* 待创建的任务. */
static void vIntegerGenerator( void *pvParameters );
static void vEventBitSettingTask( void *pvParameters );
static void vEventBitReadingTask( void *pvParameters );

/* RTOS守护任务中，可以处理的函数。这个函数打印输出传递给它的参数。*/
void vPrintStringFromDaemonTask( void *pvParameter1, uint32_t ulParameter2 );

/* (模拟)中断服务程序，在这个中断中向事件组设置一个事件位 */
static uint32_t ulEventBitSettingISR( void );

/*-----------------------------------------------------------*/

/* 声明一个事件组，在中断和任务中都可以设置该事件组的中断位 */
EventGroupHandle_t xEventGroup;

int main( void )
{
	/* 在使用事件组之前，先要创建 */
	xEventGroup = xEventGroupCreate();

	/* 创建一个向事件组设置位数据的任务 */
	xTaskCreate( vEventBitSettingTask, "BitSetter", 1000, NULL, 1, NULL );

	/* 创建一个任务等待读取事件组中的位标记 */
	xTaskCreate( vEventBitReadingTask, "BitReader", 1000, NULL, 2, NULL );

	/* 创建一个任务，周期性的产生软中断 */
	xTaskCreate( vIntegerGenerator, "IntGen", 1000, NULL, 3, NULL );

	/* 安装软中断处理函数句柄，这里的使用方式是基于FreeRTOS的移植实现。
		这种使用方式只是在FreeRTOS Windows上使用的，这里只是模拟中断  */
	vPortSetInterruptHandler( mainINTERRUPT_NUMBER, ulEventBitSettingISR );

	/* 开启调度，使任务可以执行 */
	vTaskStartScheduler();

	/* 下面的代码应该是永远不会被执行的，除非vTaskStartScheduler() 函数没有足够的堆(heap)
	空间创建空闲任务和定时器(如果配置了定时器)任务。关于堆管理和捕获堆空间耗尽的相关技术，
	在书本正文中有描述。这里的堆就是我们常说的内存，大多数情况下是指高速访问内存RAM. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

static void vEventBitSettingTask( void *pvParameters )
{
const TickType_t xDelay200ms = pdMS_TO_TICKS( 200UL ), xDontBlock = 0;

	for( ;; )
	{
		/* 进入下一轮循环之前，延迟一段时间 */
		vTaskDelay( xDelay200ms );

		/* 输出一个消息要设置bit 0了。然后将置位bit 0 */
		vPrintString( "Bit setting task -\t about to set bit 0.\r\n" );
		xEventGroupSetBits( xEventGroup, mainFIRST_TASK_BIT );

		/* 在设置其他位的之前，先延迟一段时间 */
		vTaskDelay( xDelay200ms );

		/* 输出一个消息要设置bit 1了。然后将置位bit 1 */
		vPrintString( "Bit setting task -\t about to set bit 1.\r\n" );
		xEventGroupSetBits( xEventGroup, mainSECOND_TASK_BIT );
	}
}
/*-----------------------------------------------------------*/

static uint32_t ulEventBitSettingISR( void )
{
BaseType_t xHigherPriorityTaskWoken;
/* 这个字符串不是在中断服务中打印的，而是在RTOS守护任务中打印的。
所以在这里把它声明为static的，从而确保编译器不会把字符串的分配在ISR的栈上。
（因为在守护任务中打印字符串时，ISR 的栈帧是不存在的.） */
static const char *pcString = "Bit setting ISR -\t about to set bit 2.\r\n";

	/* 一如既往的要将 xHigherPriorityTaskWoken 初始化为pdFALSE.*/
	xHigherPriorityTaskWoken = pdFALSE;

	/* 输出一个消息说要对bit 2置位。消息不能在中断中打印输出，所以
	挂起一个函数，让其在RTOS守护任务中调用该函数,来输出打印字符串 */
	xTimerPendFunctionCallFromISR( vPrintStringFromDaemonTask, ( void * ) pcString, 0, &xHigherPriorityTaskWoken );

	/* 设置事件组中的bit 2. */
	xEventGroupSetBitsFromISR( xEventGroup, mainISR_BIT, &xHigherPriorityTaskWoken );

	/* xEventGroupSetBitsFromISR() 写入到定时器命令队列.
	如果写入到定时器命令队列，使得RTOS守护任务离开阻塞态，并且如果
	RTOS守护任的优先级高于正在执行任务的优先级,那么xHigherPriorityTaskWoken的值
	将在xEventGroupSetBitsFromISR()被置为pdTRUE.

	把xHigherPriorityTaskWoken的值传给portYIELD_FROM_ISR()函数。
	如果xHigherPriorityTaskWoken的值在xSemaphoreGiveFromISR()中被置为pdTRUE了。
	那么调用portYIELD_FROM_ISR()该函数，将会导致请求上下文切换。
	如果xHigherPriorityTaskWoken的值是pdFALSE,
	那么portYIELD_FROM_ISR()的调用将没有任务效果（即不产生任何影响）。

	在windows中，该函数的portYIELD_FROM_ISR()实现，包含返回语句。
	所以这里就不显式返回一个值了。
	（我们查看portYIELD_FROM_ISR()源码，实际就是一个return语句。） */
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}
/*-----------------------------------------------------------*/

static void vEventBitReadingTask( void *pvParameters )
{
const EventBits_t xBitsToWaitFor = ( mainFIRST_TASK_BIT | mainSECOND_TASK_BIT | mainISR_BIT );
EventBits_t xEventGroupValue;

	for( ;; )
	{
		/* 阻塞等待，直到事件组中的事件位被置位 */
		xEventGroupValue = xEventGroupWaitBits( /* 要读取的事件组. */
												xEventGroup,

												/* 要用测试检查的位. */
												xBitsToWaitFor,

												/* 如果解除阻塞的条件满足，
												在退出时清除设置的位值 */
												pdTRUE,

												/* 表示不需要等待所有的位都被设置 */
												pdFALSE,

												/* 没有超时. */
												portMAX_DELAY );

		/* 打印输出每个置位的标志对应的消息. */
		if( ( xEventGroupValue & mainFIRST_TASK_BIT ) != 0 )
		{
			vPrintString( "Bit reading task -\t event bit 0 was set\r\n" );
		}

		if( ( xEventGroupValue & mainSECOND_TASK_BIT ) != 0 )
		{
			vPrintString( "Bit reading task -\t event bit 1 was set\r\n" );
		}

		if( ( xEventGroupValue & mainISR_BIT ) != 0 )
		{
			vPrintString( "Bit reading task -\t event bit 2 was set\r\n" );
		}

		vPrintString( "\r\n" );
	}
}
/*-----------------------------------------------------------*/

void vPrintStringFromDaemonTask( void *pvParameter1, uint32_t ulParameter2 )
{
	/* 打印参数pvParameter1 传入的字符串 */
	vPrintString( ( const char * ) pvParameter1 );
}
/*-----------------------------------------------------------*/

static void vIntegerGenerator( void *pvParameters )
{
TickType_t xLastExecutionTime;
const TickType_t xDelay500ms = pdMS_TO_TICKS( 500UL );

	/* 初始化 vTaskDelayUntil() 函数用到的变量 */
	xLastExecutionTime = xTaskGetTickCount();

	for( ;; )
	{
		/* 这是一个周期性任务，阻塞直到再次运行。任务每500ms执行一次.*/
		vTaskDelayUntil( &xLastExecutionTime, xDelay500ms );

		/* 产生一个模拟的中断去置位事件标志组 */
		vPortGenerateSimulatedInterrupt( mainINTERRUPT_NUMBER );
	}
}








