/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

    
	http://www.FreeRTOS.org/FAQHelp.html - 有问题吗? 阅读FAQ页面
    "我的应用不能运行, 有可能哪里出氏了?". 你定义了configASSERT()吗?

    http://www.FreeRTOS.org/support - 作为收到如此高质量软年的回馈，我们邀请你
    通过参与支持论坛，来支持我们的全球社区

    http://www.FreeRTOS.org/training - 投资于培训可以让你的团队尽可能早地提高生产力。
    现在，您可以直接从Real Time Engineers Ltd首席执行官Richard Barry
    那里获得FreeRTOS培训，他是全球领先RTO的权威机构。
    
    http://www.FreeRTOS.org/plus - 一系列FreeRTOS生态系统产品，
    包括FreeRTOS+Trace——一个不可或缺的生产力工具，一个与DOS兼容的FAT文件系统，
    以及我们的微型线程感知UDP/IP协议栈。

    http://www.FreeRTOS.org/labs - FreeRTOS新产品的孵化地。快来试试FreeRTOS+TCP，
    我们为FreeRTOS开发的新的开源TCP/IP协议栈。

    http://www.OpenRTOS.com - Real Time Engineers ltd.向High Integrity Systems ltd.
    授权FreeRTOS以OpenRTOS品牌销售。低成本的OpenRTOS许可证提供有票证的支持、赔偿和商业中间件。

    http://www.SafeRTOS.com - 高完整性系统还提供安全工程和独立SIL3认证版本，
    用于需要可证明可靠性的安全和关键任务应用。
*/

#ifndef PROJDEFS_H
#define PROJDEFS_H

/*
 * 定义任务函数原型.定义在这个文件中主要是为了
 * 在包含portable.h之前,类型就已经定义了
 */
typedef void (*TaskFunction_t)( void * );

/* 把毫秒时间单位转换成tick值.
 * 如果这里的定义不符合你的应用的要求，
 * 可以在FreeRTOSConfig.h中重新定义这个宏，把这里的复盖掉
 */
#ifndef pdMS_TO_TICKS
	#define pdMS_TO_TICKS( xTimeInMs ) ( ( TickType_t ) ( ( ( TickType_t ) ( xTimeInMs ) * ( TickType_t ) configTICK_RATE_HZ ) / ( TickType_t ) 1000 ) )
#endif

#define pdFALSE			( ( BaseType_t ) 0 )
#define pdTRUE			( ( BaseType_t ) 1 )

#define pdPASS			( pdTRUE )
#define pdFAIL			( pdFALSE )
#define errQUEUE_EMPTY	( ( BaseType_t ) 0 )
#define errQUEUE_FULL	( ( BaseType_t ) 0 )

/* FreeRTOS 错误定义. */
#define errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY	( -1 )
#define errQUEUE_BLOCKED						( -4 )
#define errQUEUE_YIELD							( -5 )

/* 用于基础数据崩溃检测的宏 */
#ifndef configUSE_LIST_DATA_INTEGRITY_CHECK_BYTES
	#define configUSE_LIST_DATA_INTEGRITY_CHECK_BYTES 0
#endif

#if( configUSE_16_BIT_TICKS == 1 )
	#define pdINTEGRITY_CHECK_VALUE 0x5a5a
#else
	#define pdINTEGRITY_CHECK_VALUE 0x5a5a5a5aUL
#endif

/* 以下errno值由FreeRTOS+组件使用，而不是FreeRTOS它本身使用. */
#define pdFREERTOS_ERRNO_NONE			0	/* 无错误 */
#define	pdFREERTOS_ERRNO_ENOENT			2	/* 没有此文件或目录 */
#define	pdFREERTOS_ERRNO_EINTR			4	/* 中断的系统调用 */
#define	pdFREERTOS_ERRNO_EIO			5	/* I/O 错误 */
#define	pdFREERTOS_ERRNO_ENXIO			6	/* 没有此设备或地址 */
#define	pdFREERTOS_ERRNO_EBADF			9	/* 错误的文件号 */
#define	pdFREERTOS_ERRNO_EAGAIN			11	/* 没有进程 */
#define	pdFREERTOS_ERRNO_EWOULDBLOCK	11	/* 操作可能阻塞 */
#define	pdFREERTOS_ERRNO_ENOMEM			12	/* 没有足够内存Not enough memory */
#define	pdFREERTOS_ERRNO_EACCES			13	/* 权限拒绝 */
#define	pdFREERTOS_ERRNO_EFAULT			14	/* 错误地址 */
#define	pdFREERTOS_ERRNO_EBUSY			16	/* 挂载设备忙 */
#define	pdFREERTOS_ERRNO_EEXIST			17	/* 文件存在 */
#define	pdFREERTOS_ERRNO_EXDEV			18	/* 跨设备链接 */
#define	pdFREERTOS_ERRNO_ENODEV			19	/* 没有此设备 */
#define	pdFREERTOS_ERRNO_ENOTDIR		20	/* 不是目录 */
#define	pdFREERTOS_ERRNO_EISDIR			21	/* 是一个目录 */
#define	pdFREERTOS_ERRNO_EINVAL			22	/* 错误参数 */
#define	pdFREERTOS_ERRNO_ENOSPC			28	/* 设备空间不足 */
#define	pdFREERTOS_ERRNO_ESPIPE			29	/* 非法的seek */
#define	pdFREERTOS_ERRNO_EROFS			30	/* 只读文件系统 */
#define	pdFREERTOS_ERRNO_EUNATCH		42	/* 协议驱动未关连 */
#define	pdFREERTOS_ERRNO_EBADE			50	/* 错误的交换 */
#define	pdFREERTOS_ERRNO_EFTYPE			79	/* 不合适的文件类型与格式 */
#define	pdFREERTOS_ERRNO_ENMFILE		89	/* 没有更多文件 */
#define	pdFREERTOS_ERRNO_ENOTEMPTY		90	/* 非空目录 */
#define	pdFREERTOS_ERRNO_ENAMETOOLONG 	91	/* 文件或路径名太长 */
#define	pdFREERTOS_ERRNO_EOPNOTSUPP		95	/* 传输端点操作不支持 */
#define	pdFREERTOS_ERRNO_ENOBUFS		105	/* 没有足够的缓冲空间 */
#define	pdFREERTOS_ERRNO_ENOPROTOOPT	109	/* 协议不支持 */
#define	pdFREERTOS_ERRNO_EADDRINUSE		112	/* 地址已使用 */
#define	pdFREERTOS_ERRNO_ETIMEDOUT		116	/* 连接超时 */
#define	pdFREERTOS_ERRNO_EINPROGRESS	119	/* 连接已在进行中 */
#define	pdFREERTOS_ERRNO_EALREADY		120	/* 套接字已连接 */
#define	pdFREERTOS_ERRNO_EADDRNOTAVAIL 	125	/* 地址不可用 */
#define	pdFREERTOS_ERRNO_EISCONN		127	/* 套接字已准备连接 */
#define	pdFREERTOS_ERRNO_ENOTCONN		128	/* 套接字未连接 */
#define	pdFREERTOS_ERRNO_ENOMEDIUM		135	/* 没有媒介插入 */
#define	pdFREERTOS_ERRNO_EILSEQ			138	/* 非法的UTF-16序列. */
#define	pdFREERTOS_ERRNO_ECANCELED		140	/* 操作取消. */

/* 以下(在小)端值由FreeRTOS+组件使用，而不是FreeRTOS它本身使用. */
#define pdFREERTOS_LITTLE_ENDIAN	0
#define pdFREERTOS_BIG_ENDIAN		1

#endif /* PROJDEFS_H */



