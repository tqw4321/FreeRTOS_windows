/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

    http://www.FreeRTOS.org/FAQHelp.html - 有问题吗? 阅读FAQ页面
    "我的应用不能运行, 有可能哪里出氏了?". 你定义了configASSERT()吗?

    http://www.FreeRTOS.org/support - 作为收到如此高质量软年的回馈，我们邀请你
    通过参与支持论坛，来支持我们的全球社区

    http://www.FreeRTOS.org/training - 投资于培训可以让你的团队尽可能早地提高生产力。
    现在，您可以直接从Real Time Engineers Ltd首席执行官Richard Barry
    那里获得FreeRTOS培训，他是全球领先RTO的权威机构。
    
    http://www.FreeRTOS.org/plus - 一系列FreeRTOS生态系统产品，
    包括FreeRTOS+Trace——一个不可或缺的生产力工具，一个与DOS兼容的FAT文件系统，
    以及我们的微型线程感知UDP/IP协议栈。

    http://www.FreeRTOS.org/labs - FreeRTOS新产品的孵化地。快来试试FreeRTOS+TCP，
    我们为FreeRTOS开发的新的开源TCP/IP协议栈。

    http://www.OpenRTOS.com - Real Time Engineers ltd.向High Integrity Systems ltd.
    授权FreeRTOS以OpenRTOS品牌销售。低成本的OpenRTOS许可证提供有票证的支持、赔偿和商业中间件。

    http://www.SafeRTOS.com - 高完整性系统还提供安全工程和独立SIL3认证版本，
    用于需要可证明可靠性的安全和关键任务应用。

    1 tab == 4 spaces!
*/

/* 标准头文件包含. */
#include <stdlib.h>

/* 定义MPU_WRAPPERS_INCLUDED_FROM_API_FILE 这个宏，是为了当应用中包含 task.h时，
   阻止在task.h 中把所有API函数重定义为MPU包裹的函数。*/
#define MPU_WRAPPERS_INCLUDED_FROM_API_FILE

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

#if ( INCLUDE_xTimerPendFunctionCall == 1 ) && ( configUSE_TIMERS == 0 )
	#error configUSE_TIMERS must be set to 1 to make the xTimerPendFunctionCall() function available.
#endif

/* 这个宏放在这里是为了抑制MISRA合规检查中的e961 和 e750 异常。因为MPU移植要求
MPU_WRAPPERS_INCLUDED_FROM_API_FILE 宏定义在那个文件中，而不能定义在本文件中。*/
#undef MPU_WRAPPERS_INCLUDED_FROM_API_FILE /*lint !e961 !e750. */


/* 如果应用没有配置要包含定时器的功能，那么整个源码将会被跳过
 这个#if 在文件尾有一个结束。如果你想要使用定时器的功能，
 就要在FreeRTOSConfig.h中把configUSE_TIMERS定义为1*/
#if ( configUSE_TIMERS == 1 )

/* 杂项定义. */
#define tmrNO_DELAY		( TickType_t ) 0U

/* 定时器本身的定义. */
typedef struct tmrTimerControl
{
	const char				*pcTimerName;		/*<< 文本名称。内核不使用它，它只是为了方便调试. */ /*lint !e971 只允许字符串和单个字符使用非限定字符类型. */
	ListItem_t				xTimerListItem;		/*<< 所有内核功能用于事件管理的标准链接列表项. */
	TickType_t				xTimerPeriodInTicks;/*<< 定时器频率. */
	UBaseType_t				uxAutoReload;		/*<< 如果计时器过期后应自动重新启动，则设置为pdTRUE。如果计时器实际上是一次性计时器，则设置为pdFALSE。 */
	void 					*pvTimerID;			/*<< 标识计时器的ID。当同一个回调用于多个计时器时，可以标识计时器。 */
	TimerCallbackFunction_t	pxCallbackFunction;	/*<< 计时器过期时将调用的函数. */
	#if( configUSE_TRACE_FACILITY == 1 )
		UBaseType_t			uxTimerNumber;		/*<< 由跟踪工具（如FreeRTOS+trace）分配的ID */
	#endif

	#if( ( configSUPPORT_STATIC_ALLOCATION == 1 ) && ( configSUPPORT_DYNAMIC_ALLOCATION == 1 ) )
		uint8_t 			ucStaticallyAllocated; /*<< 如果计时器是静态创建的，则设置为pdTRUE，因此如果稍后删除计时器，则不会再次尝试释放内存. */
	#endif
} xTIMER;

/* 旧的xTIMER名称维护在上面，然后重定义为新的Timer_t类型，以使用旧的内核感知调试可以使用 */
typedef xTIMER Timer_t;

/* 定义可以从定时器队列中接收和发送的消息
两种可以队列化的消息类型--操作定时器的消息和请求执行非定时器相关的回调函数消息。
这两种消息分别定义为两种不同的类型，分别为xTimerParametersType和
xCallbackParametersType */
typedef struct tmrTimerParameters
{
	TickType_t			xMessageValue;		/*<< 命令子集使用的可选值，例如更改计时器的周期时. */
	Timer_t *			pxTimer;			/*<< 将应用于命令的定时器. */
} TimerParameter_t;


typedef struct tmrCallbackParameters
{
	PendedFunction_t	pxCallbackFunction;	/* << 要执行的回调函数. */
	void *pvParameter1;						/* << 将用作回调函数第一个参数的值. */
	uint32_t ulParameter2;					/* << 将用作回调函数第二个参数的值。 */
} CallbackParameters_t;

/* 包含两种消息类型和标识符的结构,标识符用于确定哪种消息类型有效. */
typedef struct tmrTimerQueueMessage
{
	BaseType_t			xMessageID;			/*<< 发送到计时器服务任务的命令. */
	union
	{
		TimerParameter_t xTimerParameters;

		/* 如果不使用该功能，就不包含它，因为它使用结构变得更大(定时器队列也相应变大) */
		#if ( INCLUDE_xTimerPendFunctionCall == 1 )
			CallbackParameters_t xCallbackParameters;
		#endif /* INCLUDE_xTimerPendFunctionCall */
	} u;
} DaemonTaskMessage_t;

/*lint -e956 已经使用手动分析和检查来确定哪些静态变量必须声明为volatile. */

/* 存储活动计时器的链表，定时器按过期时间顺序引用，最近的过期时间在列表的前面。
只有定时器服务任务可以访问这些链表 */
PRIVILEGED_DATA static List_t xActiveTimerList1;
PRIVILEGED_DATA static List_t xActiveTimerList2;
PRIVILEGED_DATA static List_t *pxCurrentTimerList;
PRIVILEGED_DATA static List_t *pxOverflowTimerList;

/* 用于向定时器服务任务发送命令的队列。 */
PRIVILEGED_DATA static QueueHandle_t xTimerQueue = NULL;
PRIVILEGED_DATA static TaskHandle_t xTimerTaskHandle = NULL;

/*lint +e956 */

/*-----------------------------------------------------------*/

#if( configSUPPORT_STATIC_ALLOCATION == 1 )

	/* 如果支持静态分配，那么应用必须提供下面的回调函数--该函数允许应用
	提供内存,给定时器服务任务做任务栈和TCB之用。*/
	extern void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize );

#endif

/*
 * 初始化计时器服务任务使用的基础结构（如果尚未初始化）。
 */
static void prvCheckForValidListAndQueue( void ) PRIVILEGED_FUNCTION;

/*
 * 定时器服务任务(守护).定时器的功能都由这个任务控制。
 * 其他任务通过xTimerQueue队列与该任务通信
 */
static void prvTimerTask( void *pvParameters ) PRIVILEGED_FUNCTION;

/*
 * 由定时器服务任务调用，用于解释和处理来自定时器队列的命令
 */
static void prvProcessReceivedCommands( void ) PRIVILEGED_FUNCTION;

/*
 * 根据过期时间是否导致定时器计数器溢出，
 * 将定时器插入xActiveTimerList1或xActiveTimerList2。
 */
static BaseType_t prvInsertTimerInActiveList( Timer_t * const pxTimer, const TickType_t xNextExpiryTime, const TickType_t xTimeNow, const TickType_t xCommandTime ) PRIVILEGED_FUNCTION;

/*
 * 活动定时器已达到过期时间。如果定时器是自动重新加载计时器，
 * 重新加载它，然后调用它的回调函数。
 */
static void prvProcessExpiredTimer( const TickType_t xNextExpireTime, const TickType_t xTimeNow ) PRIVILEGED_FUNCTION;

/*
 * (tick)滴答计数器忆溢出。在确保当前定时器链表不再引用某些定时器时，切换定时器列表。
 * The tick count has overflowed.  Switch the timer lists after ensuring the
 * current timer list does not still reference some timers.
 */
static void prvSwitchTimerLists( void ) PRIVILEGED_FUNCTION;

/*
 * 获取当前tick计数值，自从上次调用prvSampleTimeNow()后，tick记数发生溢出
 * 将 *pxTimerListsWereSwitched 设为pdTRUE
 */
static TickType_t prvSampleTimeNow( BaseType_t * const pxTimerListsWereSwitched ) PRIVILEGED_FUNCTION;

/*
 * 如果定时器链表包含活动定时器，那么返回最第一个即将到期的定时器的到期时间，
 * 并且把 *pxListWasEmpty 设为 pdFALSE.如果不包含任何定时器那么返回0,并且将
 * *pxListWasEmpty设为pdTRUE.
 */
static TickType_t prvGetNextExpireTime( BaseType_t * const pxListWasEmpty ) PRIVILEGED_FUNCTION;

/*
 * 如果一个定时器到期，处理它。否则，阻塞定时器任务，直到有定时器到期或者收到命令
 */
static void prvProcessTimerOrBlockTask( const TickType_t xNextExpireTime, BaseType_t xListWasEmpty ) PRIVILEGED_FUNCTION;

/*
 * 在以静态或动态方式为定时器分配内存后调用，以填充结构成员。
 */
static void prvInitialiseNewTimer(	const char * const pcTimerName,
									const TickType_t xTimerPeriodInTicks,
									const UBaseType_t uxAutoReload,
									void * const pvTimerID,
									TimerCallbackFunction_t pxCallbackFunction,
									Timer_t *pxNewTimer ) PRIVILEGED_FUNCTION; /*lint !e971 只允许字符串和单个字符使用非限定字符类型. */
/*-----------------------------------------------------------*/

BaseType_t xTimerCreateTimerTask( void )
{
BaseType_t xReturn = pdFAIL;

	/* 如果configUSE_TIMERS设置为1，则在启动调度器时调用此函数。
	   检查计时器服务任务使用的基础结构是否已创建/初始化。
	   如果已经创建了计时器，则已执行初始化。 */
	prvCheckForValidListAndQueue();

	if( xTimerQueue != NULL )
	{
		#if( configSUPPORT_STATIC_ALLOCATION == 1 )
		{
			StaticTask_t *pxTimerTaskTCBBuffer = NULL;
			StackType_t *pxTimerTaskStackBuffer = NULL;
			uint32_t ulTimerTaskStackSize;

			vApplicationGetTimerTaskMemory( &pxTimerTaskTCBBuffer, &pxTimerTaskStackBuffer, &ulTimerTaskStackSize );
			xTimerTaskHandle = xTaskCreateStatic(	prvTimerTask,
													"Tmr Svc",
													ulTimerTaskStackSize,
													NULL,
													( ( UBaseType_t ) configTIMER_TASK_PRIORITY ) | portPRIVILEGE_BIT,
													pxTimerTaskStackBuffer,
													pxTimerTaskTCBBuffer );

			if( xTimerTaskHandle != NULL )
			{
				xReturn = pdPASS;
			}
		}
		#else
		{
			xReturn = xTaskCreate(	prvTimerTask,
									"Tmr Svc",
									configTIMER_TASK_STACK_DEPTH,
									NULL,
									( ( UBaseType_t ) configTIMER_TASK_PRIORITY ) | portPRIVILEGE_BIT,
									&xTimerTaskHandle );
		}
		#endif /* configSUPPORT_STATIC_ALLOCATION */
	}
	else
	{
		mtCOVERAGE_TEST_MARKER();
	}

	configASSERT( xReturn );
	return xReturn;
}
/*-----------------------------------------------------------*/

/* 
 * add by tqw 
 * 1:为定时器结构体Timer_t分配内存
 * 2:并用创建参数，初始化分配到的内存结构体。
 * 3:返回这个结构体指针
*/

#if( configSUPPORT_DYNAMIC_ALLOCATION == 1 )

	TimerHandle_t xTimerCreate(	const char * const pcTimerName,
								const TickType_t xTimerPeriodInTicks,
								const UBaseType_t uxAutoReload,
								void * const pvTimerID,
								TimerCallbackFunction_t pxCallbackFunction ) /*lint !e971 只允许字符串和单个字符使用非限定字符类型. */
	{
	Timer_t *pxNewTimer;

		pxNewTimer = ( Timer_t * ) pvPortMalloc( sizeof( Timer_t ) );

		if( pxNewTimer != NULL )
		{
			prvInitialiseNewTimer( pcTimerName, xTimerPeriodInTicks, uxAutoReload, pvTimerID, pxCallbackFunction, pxNewTimer );

			#if( configSUPPORT_STATIC_ALLOCATION == 1 )
			{
				/* 定时器可以动态或静态创建。所以注意这里定时器是动态创建，
				这里设为pdFALSE，后续删除时释放内存. */
				pxNewTimer->ucStaticallyAllocated = pdFALSE;
			}
			#endif /* configSUPPORT_STATIC_ALLOCATION */
		}

		return pxNewTimer;
	}

#endif /* configSUPPORT_STATIC_ALLOCATION */
/*-----------------------------------------------------------*/

#if( configSUPPORT_STATIC_ALLOCATION == 1 )

	TimerHandle_t xTimerCreateStatic(	const char * const pcTimerName,
										const TickType_t xTimerPeriodInTicks,
										const UBaseType_t uxAutoReload,
										void * const pvTimerID,
										TimerCallbackFunction_t pxCallbackFunction,
										StaticTimer_t *pxTimerBuffer ) /*lint !e971 只允许字符串和单个字符使用非限定字符类型. */
	{
	Timer_t *pxNewTimer;

		#if( configASSERT_DEFINED == 1 )
		{
			/* 检查StaticTimer_t类的size与Timer_t类的size 是否一样大*/
			volatile size_t xSize = sizeof( StaticTimer_t );
			configASSERT( xSize == sizeof( Timer_t ) );
		}
		#endif /* configASSERT_DEFINED */

		/* StaticTimer_t结构类型的指针必须提供 */
		configASSERT( pxTimerBuffer );
		pxNewTimer = ( Timer_t * ) pxTimerBuffer; /*lint !e740 不寻常的转换是可以的，因为结构设计为具有相同的对齐方式，并且大小由断言检查. */

		if( pxNewTimer != NULL )
		{
			prvInitialiseNewTimer( pcTimerName, xTimerPeriodInTicks, uxAutoReload, pvTimerID, pxCallbackFunction, pxNewTimer );

			#if( configSUPPORT_DYNAMIC_ALLOCATION == 1 )
			{
				/* 定时器可以动态或静态创建。所以注意这里定时器是静态创建，
				这里设为pdTRUE，后续删除时不释放内存. */
				pxNewTimer->ucStaticallyAllocated = pdTRUE;
			}
			#endif /* configSUPPORT_DYNAMIC_ALLOCATION */
		}

		return pxNewTimer;
	}

#endif /* configSUPPORT_STATIC_ALLOCATION */
/*-----------------------------------------------------------*/

static void prvInitialiseNewTimer(	const char * const pcTimerName,
									const TickType_t xTimerPeriodInTicks,
									const UBaseType_t uxAutoReload,
									void * const pvTimerID,
									TimerCallbackFunction_t pxCallbackFunction,
									Timer_t *pxNewTimer ) /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
{
	/* 定时周期必须要大于0 ,0不是合法的值. */
	configASSERT( ( xTimerPeriodInTicks > 0 ) );

	if( pxNewTimer != NULL )
	{
		/* 确保定时器服务/守护任务使用的基础数据结构被始化，如果没有则会初始化一次。
		   如果初始化了就不会再初始化.
		 */
		prvCheckForValidListAndQueue();

		/* 使用函数参数初始化结构成员. */
		pxNewTimer->pcTimerName = pcTimerName;
		pxNewTimer->xTimerPeriodInTicks = xTimerPeriodInTicks;
		pxNewTimer->uxAutoReload = uxAutoReload;
		pxNewTimer->pvTimerID = pvTimerID;
		pxNewTimer->pxCallbackFunction = pxCallbackFunction;
		vListInitialiseItem( &( pxNewTimer->xTimerListItem ) );
		traceTIMER_CREATE( pxNewTimer );
	}
}
/*-----------------------------------------------------------*/

BaseType_t xTimerGenericCommand( TimerHandle_t xTimer, const BaseType_t xCommandID, const TickType_t xOptionalValue, BaseType_t * const pxHigherPriorityTaskWoken, const TickType_t xTicksToWait )
{
BaseType_t xReturn = pdFAIL;
DaemonTaskMessage_t xMessage;

	configASSERT( xTimer );

	/* 向计时器服务任务发送消息以对特定计时器定义执行特定操作. */
	if( xTimerQueue != NULL )
	{
		/* 发送命令给定时器服务程序. */
		xMessage.xMessageID = xCommandID;
		xMessage.u.xTimerParameters.xMessageValue = xOptionalValue;
		xMessage.u.xTimerParameters.pxTimer = ( Timer_t * ) xTimer;
		/* add by tqw 
			1.根据命令的范围使用中断或者非中断版本的发送函数,
			 注意有一个中断命令tmrCOMMAND_EXECUTE_CALLBACK_FROM_ISR是小于tmrFIRST_FROM_ISR_COMMAND,
			 但是这条命令并没有通过这个函数发送，而是专门写了一个发送函数xTimerPendFunctionCallFromISR()
			 所以这个条件判断没有问题。
			2.这个函数xTimerGenericCommand 没有直接调用，都是通过宏来调用的，这样做很多好处
			  	a>把参数固化下来，减少参数的填写，从而降低写错参数的概率，增加代码的可读性
			  	b>方便应用代码升级。比如这个函数增加了参数或者改变了函数名称。由于应用层使用的是宏
			  	  所以，因此只需要保持宏的语义相同，就可以在不修改应用的情况下更新库就能升级成功。
			3.如果调度器没有启动之前，调用了timer相关API函数，那么阻塞时间将忽略，直接使用tmrNO_DELAY
		*/
		if( xCommandID < tmrFIRST_FROM_ISR_COMMAND )
		{
			if( xTaskGetSchedulerState() == taskSCHEDULER_RUNNING )
			{
				xReturn = xQueueSendToBack( xTimerQueue, &xMessage, xTicksToWait );
			}
			else
			{
				xReturn = xQueueSendToBack( xTimerQueue, &xMessage, tmrNO_DELAY );
			}
		}
		else
		{
			xReturn = xQueueSendToBackFromISR( xTimerQueue, &xMessage, pxHigherPriorityTaskWoken );
		}

		traceTIMER_COMMAND_SEND( xTimer, xCommandID, xOptionalValue, xReturn );
	}
	else
	{
		mtCOVERAGE_TEST_MARKER();
	}

	return xReturn;
}
/*-----------------------------------------------------------*/

TaskHandle_t xTimerGetTimerDaemonTaskHandle( void )
{
	/* 如果在调度器启动之前调用该函数，那么xTimerTaskHandle就为NULL */
	configASSERT( ( xTimerTaskHandle != NULL ) );
	return xTimerTaskHandle;
}
/*-----------------------------------------------------------*/

TickType_t xTimerGetPeriod( TimerHandle_t xTimer )
{
Timer_t *pxTimer = ( Timer_t * ) xTimer;

	configASSERT( xTimer );
	return pxTimer->xTimerPeriodInTicks;
}
/*-----------------------------------------------------------*/

TickType_t xTimerGetExpiryTime( TimerHandle_t xTimer )
{
Timer_t * pxTimer = ( Timer_t * ) xTimer;
TickType_t xReturn;

	configASSERT( xTimer );
	xReturn = listGET_LIST_ITEM_VALUE( &( pxTimer->xTimerListItem ) );
	return xReturn;
}
/*-----------------------------------------------------------*/

const char * pcTimerGetName( TimerHandle_t xTimer ) /*lint !e971 只允许字符串和单个字符使用非限定字符类型. */
{
Timer_t *pxTimer = ( Timer_t * ) xTimer;

	configASSERT( xTimer );
	return pxTimer->pcTimerName;
}
/*-----------------------------------------------------------*/

static void prvProcessExpiredTimer( const TickType_t xNextExpireTime, const TickType_t xTimeNow )
{
BaseType_t xResult;
Timer_t * const pxTimer = ( Timer_t * ) listGET_OWNER_OF_HEAD_ENTRY( pxCurrentTimerList );

	/* 把定时器从激活定时器链表中移除。该链表首先已经检查了不是空的. */
	( void ) uxListRemove( &( pxTimer->xTimerListItem ) );
	traceTIMER_EXPIRED( pxTimer );

	/* 如果定时器是自动重新加载的定时器，那么计算下一次到期时间并把它重新插入到激话定时器链表中*/
	if( pxTimer->uxAutoReload == ( UBaseType_t ) pdTRUE )
	{
		/* The timer is inserted into a list using a time relative to anything
		other than the current time.  It will therefore be inserted into the
		correct list relative to the time this task thinks it is now. */
		if( prvInsertTimerInActiveList( pxTimer, ( xNextExpireTime + pxTimer->xTimerPeriodInTicks ), xTimeNow, xNextExpireTime ) != pdFALSE )
		{
			/* The timer expired before it was added to the active timer
			list.  Reload it now.  */
			xResult = xTimerGenericCommand( pxTimer, tmrCOMMAND_START_DONT_TRACE, xNextExpireTime, NULL, tmrNO_DELAY );
			configASSERT( xResult );
			( void ) xResult;
		}
		else
		{
			mtCOVERAGE_TEST_MARKER();
		}
	}
	else
	{
		mtCOVERAGE_TEST_MARKER();
	}

	/* Call the timer callback. */
	pxTimer->pxCallbackFunction( ( TimerHandle_t ) pxTimer );
}
/*-----------------------------------------------------------*/

static void prvTimerTask( void *pvParameters )
{
TickType_t xNextExpireTime;
BaseType_t xListWasEmpty;

	/* 这一句只是为了辟免编译器警告. */
	( void ) pvParameters;

	#if( configUSE_DAEMON_TASK_STARTUP_HOOK == 1 )
	{
		extern void vApplicationDaemonTaskStartupHook( void );

		/* 允许应用程序编写器在任务开始执行时在此任务的上下文中执行某些代码。
		   如果应用程序包含初始化代码，这将有助于在调度程序启动后执行这些代码。*/
		vApplicationDaemonTaskStartupHook();
	}
	#endif /* configUSE_DAEMON_TASK_STARTUP_HOOK */

	for( ;; )
	{
		/*查询定时器列表看是否有包含定时器。如果有就获取下一个即将到期的定时器的时间 */
		xNextExpireTime = prvGetNextExpireTime( &xListWasEmpty );

		/* 如果定时器到期，处理它。否则，阻塞任务直到有定时器到期或者收到一个命令 */
		prvProcessTimerOrBlockTask( xNextExpireTime, xListWasEmpty );

		/* 清空命令队列. */
		prvProcessReceivedCommands();
	}
}
/*-----------------------------------------------------------*/

static void prvProcessTimerOrBlockTask( const TickType_t xNextExpireTime, BaseType_t xListWasEmpty )
{
TickType_t xTimeNow;
BaseType_t xTimerListsWereSwitched;

	vTaskSuspendAll();
	{
		/* 现在获取时间，以评估计时器是否已过期。
		   如果获取时间会导致列表切换，则不要处理此计时器，
		   因为切换列表时留在列表中的任何计时器都
		   将在prvSampleTimeNow（）函数中处理。 */
		xTimeNow = prvSampleTimeNow( &xTimerListsWereSwitched );
		if( xTimerListsWereSwitched == pdFALSE )
		{
			/* 滴答计时器没有溢出，是否有定时器到期? */
			if( ( xListWasEmpty == pdFALSE ) && ( xNextExpireTime <= xTimeNow ) )
			{
				( void ) xTaskResumeAll();
				prvProcessExpiredTimer( xNextExpireTime, xTimeNow );
			}
			else
			{
				/* 定时器没有溢出，并且也没有到达下一个到期时间。
 				因此任务需要阻塞等待下一个定时时间过期或者收到一个命令
 				--无论那一个先到都可以。只有当定时器列表为空，或者
 				xNextExpireTime > xTimeNow时，下面的代码才会执行. */
				if( xListWasEmpty != pdFALSE )
				{
					/* 当前定时器链表为空-溢出链表是否也为空? */
					xListWasEmpty = listLIST_IS_EMPTY( pxOverflowTimerList );
				}

				vQueueWaitForMessageRestricted( xTimerQueue, ( xNextExpireTime - xTimeNow ), xListWasEmpty );

				if( xTaskResumeAll() == pdFALSE )
				{
					/* yeild等待一个命令到达或阻塞时间到期。如果
					命令在退出临界区与这个yield之间到达，那么这个yeild不会导致任务
					阻塞 */
					portYIELD_WITHIN_API();
				}
				else
				{
					mtCOVERAGE_TEST_MARKER();
				}
			}
		}
		else
		{
			( void ) xTaskResumeAll();
		}
	}
}
/*-----------------------------------------------------------*/

static TickType_t prvGetNextExpireTime( BaseType_t * const pxListWasEmpty )
{
TickType_t xNextExpireTime;

	/* 定时器按时间顺序存储在链表中，链表头引用的是最先到期的定时器。
	   获取最先到期的定时器的时间。如果没有其他激活的定时器，把下一个到期
       时间设为0。当tick记数溢出时，这样会导至该任务退出阻塞态，这时定时器
       列表将被切换，并且可以重新评估下一个到期时间 */
	*pxListWasEmpty = listLIST_IS_EMPTY( pxCurrentTimerList );
	if( *pxListWasEmpty == pdFALSE )
	{
		xNextExpireTime = listGET_ITEM_VALUE_OF_HEAD_ENTRY( pxCurrentTimerList );
	}
	else
	{
		/* 确保在计时计数滚动时任务未被阻止. */
		xNextExpireTime = ( TickType_t ) 0U;
	}

	return xNextExpireTime;
}
/*-----------------------------------------------------------*/

static TickType_t prvSampleTimeNow( BaseType_t * const pxTimerListsWereSwitched )
{
TickType_t xTimeNow;
PRIVILEGED_DATA static TickType_t xLastTime = ( TickType_t ) 0U; /*lint !e956 变量只能对一个任务访问. */

	xTimeNow = xTaskGetTickCount();

	if( xTimeNow < xLastTime )
	{
		prvSwitchTimerLists();
		*pxTimerListsWereSwitched = pdTRUE;
	}
	else
	{
		*pxTimerListsWereSwitched = pdFALSE;
	}

	xLastTime = xTimeNow;

	return xTimeNow;
}
/*-----------------------------------------------------------*/

static BaseType_t prvInsertTimerInActiveList( Timer_t * const pxTimer, const TickType_t xNextExpiryTime, const TickType_t xTimeNow, const TickType_t xCommandTime )
{
BaseType_t xProcessTimerNow = pdFALSE;

	listSET_LIST_ITEM_VALUE( &( pxTimer->xTimerListItem ), xNextExpiryTime );
	listSET_LIST_ITEM_OWNER( &( pxTimer->xTimerListItem ), pxTimer );

	if( xNextExpiryTime <= xTimeNow )
	{
		/* 从发出启动/重置定时器的命令到处理该命令的时间之间是否已过了到期时间？? */
		if( ( ( TickType_t ) ( xTimeNow - xCommandTime ) ) >= pxTimer->xTimerPeriodInTicks ) /*lint !e961 MISRA异常，因为强制转换仅对某些移植是冗余的. */
		{
			/* 发出命令和正在处理的命令之间的时间实际上超过了定时器周期.  */
			xProcessTimerNow = pdTRUE;
		}
		else
		{
			vListInsert( pxOverflowTimerList, &( pxTimer->xTimerListItem ) );
		}
	}
	else
	{
		if( ( xTimeNow < xCommandTime ) && ( xNextExpiryTime >= xCommandTime ) )
		{
			/*如果自发出命令以来，tick计数已溢出，但过期时间未到，则计时器必须已超过其到期时间，应立即处理. */
			xProcessTimerNow = pdTRUE;
		}
		else
		{
			vListInsert( pxCurrentTimerList, &( pxTimer->xTimerListItem ) );
		}
	}

	return xProcessTimerNow;
}
/*-----------------------------------------------------------*/

static void	prvProcessReceivedCommands( void )
{
DaemonTaskMessage_t xMessage;
Timer_t *pxTimer;
BaseType_t xTimerListsWereSwitched, xResult;
TickType_t xTimeNow;

	while( xQueueReceive( xTimerQueue, &xMessage, tmrNO_DELAY ) != pdFAIL ) /*lint !e603 xMessage不必初始化，因为它是传递出去的，而不是传入的，除非xQueueReceive（）返回pdTRUE，否则不使用它. */
	{
		#if ( INCLUDE_xTimerPendFunctionCall == 1 )
		{
			/* 负命令是挂起的函数的调用而不是定时器命令,-1,-2 功能都是挂起函数调用，
			一个是中断中发送的命令，一个是非中断中发送的命令.
			这里接收到这个命令后，就立即执行挂起的函数。
			*/
			if( xMessage.xMessageID < ( BaseType_t ) 0 )
			{
				const CallbackParameters_t * const pxCallback = &( xMessage.u.xCallbackParameters );

				/* 定时器将使用xCallbackParameters来执行一个回调，所以检查回调不是NULL */
				configASSERT( pxCallback );

				/* 调用这个函数. */
				pxCallback->pxCallbackFunction( pxCallback->pvParameter1, pxCallback->ulParameter2 );
			}
			else
			{
				mtCOVERAGE_TEST_MARKER();
			}
		}
		#endif /* INCLUDE_xTimerPendFunctionCall */

		/* 正的命令是定时器命令，而不是挂起的函数调用. */
		if( xMessage.xMessageID >= ( BaseType_t ) 0 )
		{
			/* 这些消息使用xTimerParameters成员用于软定时器.(因为这里使用了联合体union) */
			pxTimer = xMessage.u.xTimerParameters.pxTimer;

			if( listIS_CONTAINED_WITHIN( NULL, &( pxTimer->xTimerListItem ) ) == pdFALSE )
			{
				/* 定时器在某个链表(一个是活动链表，一个是溢出链表)中,把该定时器从链表中移除它. */
				( void ) uxListRemove( &( pxTimer->xTimerListItem ) );
			}
			else
			{
				mtCOVERAGE_TEST_MARKER();
			}

			traceTIMER_COMMAND_RECEIVED( pxTimer, xMessage.xMessageID, xMessage.u.xTimerParameters.xMessageValue );

			/* In this case the xTimerListsWereSwitched parameter is not used, but
			it must be present in the function call.  prvSampleTimeNow() must be
			called after the message is received from xTimerQueue so there is no
			possibility of a higher priority task adding a message to the message
			queue with a time that is ahead of the timer daemon task (because it
			pre-empted the timer daemon task after the xTimeNow value was set). */
			xTimeNow = prvSampleTimeNow( &xTimerListsWereSwitched );

			switch( xMessage.xMessageID )
			{
				case tmrCOMMAND_START :
			    case tmrCOMMAND_START_FROM_ISR :
			    case tmrCOMMAND_RESET :
			    case tmrCOMMAND_RESET_FROM_ISR :
				case tmrCOMMAND_START_DONT_TRACE :
					/* 启动或重启定时器. */
					if( prvInsertTimerInActiveList( pxTimer,  xMessage.u.xTimerParameters.xMessageValue + pxTimer->xTimerPeriodInTicks, xTimeNow, xMessage.u.xTimerParameters.xMessageValue ) != pdFALSE )
					{
						/* 定时器在被加入到激活链表之前就已经到期了，现就是处理到期事件. */
						pxTimer->pxCallbackFunction( ( TimerHandle_t ) pxTimer );
						traceTIMER_EXPIRED( pxTimer );

						if( pxTimer->uxAutoReload == ( UBaseType_t ) pdTRUE )
						{
							xResult = xTimerGenericCommand( pxTimer, tmrCOMMAND_START_DONT_TRACE, xMessage.u.xTimerParameters.xMessageValue + pxTimer->xTimerPeriodInTicks, NULL, tmrNO_DELAY );
							configASSERT( xResult );
							( void ) xResult;
						}
						else
						{
							mtCOVERAGE_TEST_MARKER();
						}
					}
					else
					{
						mtCOVERAGE_TEST_MARKER();
					}
					break;

				case tmrCOMMAND_STOP :
				case tmrCOMMAND_STOP_FROM_ISR :
					/* 计时器已从活动列表中删除。这里没什么可做的. */
					break;

				case tmrCOMMAND_CHANGE_PERIOD :
				case tmrCOMMAND_CHANGE_PERIOD_FROM_ISR :
					pxTimer->xTimerPeriodInTicks = xMessage.u.xTimerParameters.xMessageValue;
					configASSERT( ( pxTimer->xTimerPeriodInTicks > 0 ) );

					/* The new period does not really have a reference, and can
					be longer or shorter than the old one.  The command time is
					therefore set to the current time, and as the period cannot
					be zero the next expiry time can only be in the future,
					meaning (unlike for the xTimerStart() case above) there is
					no fail case that needs to be handled here. */
					( void ) prvInsertTimerInActiveList( pxTimer, ( xTimeNow + pxTimer->xTimerPeriodInTicks ), xTimeNow, xTimeNow );
					break;

				case tmrCOMMAND_DELETE :
					/* 计时器已从活动列表中删除，如果内存是动态分配的，只需要释放内存即可。 */
					#if( ( configSUPPORT_DYNAMIC_ALLOCATION == 1 ) && ( configSUPPORT_STATIC_ALLOCATION == 0 ) )
					{
						/* 只能动态分配计时器-再次释放它. */
						vPortFree( pxTimer );
					}
					#elif( ( configSUPPORT_DYNAMIC_ALLOCATION == 1 ) && ( configSUPPORT_STATIC_ALLOCATION == 1 ) )
					{
						/* 计时器可能是静态或动态分配的，因此请在尝试释放内存之前进行检查. */
						if( pxTimer->ucStaticallyAllocated == ( uint8_t ) pdFALSE )
						{
							vPortFree( pxTimer );
						}
						else
						{
							mtCOVERAGE_TEST_MARKER();
						}
					}
					#endif /* configSUPPORT_DYNAMIC_ALLOCATION */
					break;

				default	:
					/* 代码不能执行到这里. */
					break;
			}
		}
	}
}
/*-----------------------------------------------------------*/

static void prvSwitchTimerLists( void )
{
TickType_t xNextExpireTime, xReloadTime;
List_t *pxTemp;
Timer_t *pxTimer;
BaseType_t xResult;

	/* tick计数器已经溢出。定时器列表必须切换。
	   如果当前定时器列表中仍有定时器被引用，它们肯定过期了(理解这句话很重要)，
	   必须在列表切换之前处理处理掉。*/
	while( listLIST_IS_EMPTY( pxCurrentTimerList ) == pdFALSE )
	{
		xNextExpireTime = listGET_ITEM_VALUE_OF_HEAD_ENTRY( pxCurrentTimerList );

		/* 把定时器从当前列表中移除 */
		pxTimer = ( Timer_t * ) listGET_OWNER_OF_HEAD_ENTRY( pxCurrentTimerList );
		( void ) uxListRemove( &( pxTimer->xTimerListItem ) );
		traceTIMER_EXPIRED( pxTimer );

		/* 执行它的回调函数，如果该定时器是自动重载定时器，那么就发送重启定时器命令。
		它不能在这时重启，因为链表还没有交换(所以只能发条命令). */
		pxTimer->pxCallbackFunction( ( TimerHandle_t ) pxTimer );

		if( pxTimer->uxAutoReload == ( UBaseType_t ) pdTRUE )
		{
			/* 计算重载时间，并且如果重新加载值，使得定时器将被插入到同一个定时器链表，
			那么它已经过期了，因此该定时器应被重新插入到当前列表，以使得它在该循环中
			再次被处理。否则的话发送一个重启命令，以确保在交换链表后，该定时器只能插入
			到一个链表中。
			译注:
			在溢出的时候 xNextExpireTime的值是一个负值时。下面的条件才成立			
			xReloadTime = ( xNextExpireTime + pxTimer->xTimerPeriodInTicks );
			if( xReloadTime > xNextExpireTime )
			举例:假如最大值是500,定时器的过期时间是100,当前计时是450时，重载了定时器
			这时得到的xNextExpireTime就是-50.理解这个溢出很关键。
			实际的溢出是 TickType_t 的最大值 */
			xReloadTime = ( xNextExpireTime + pxTimer->xTimerPeriodInTicks );
			if( xReloadTime > xNextExpireTime )
			{
				listSET_LIST_ITEM_VALUE( &( pxTimer->xTimerListItem ), xReloadTime );
				listSET_LIST_ITEM_OWNER( &( pxTimer->xTimerListItem ), pxTimer );
				vListInsert( pxCurrentTimerList, &( pxTimer->xTimerListItem ) );
			}
			else
			{
				xResult = xTimerGenericCommand( pxTimer, tmrCOMMAND_START_DONT_TRACE, xNextExpireTime, NULL, tmrNO_DELAY );
				configASSERT( xResult );
				( void ) xResult;
			}
		}
		else
		{
			mtCOVERAGE_TEST_MARKER();
		}
	}

	pxTemp = pxCurrentTimerList;
	pxCurrentTimerList = pxOverflowTimerList;
	pxOverflowTimerList = pxTemp;
}
/*-----------------------------------------------------------*/

static void prvCheckForValidListAndQueue( void )
{
	/* 检查活动定时器链表是否被引用，以及用于与定时器服务任务通信的
	   队列是否被初始化 */
	taskENTER_CRITICAL();
	{
		if( xTimerQueue == NULL )
		{
			vListInitialise( &xActiveTimerList1 );
			vListInitialise( &xActiveTimerList2 );
			pxCurrentTimerList = &xActiveTimerList1;
			pxOverflowTimerList = &xActiveTimerList2;

			#if( configSUPPORT_STATIC_ALLOCATION == 1 )
			{
				/* configSUPPORT_DYNAMIC_ALLOCATION为0时，
				   定时器队列使用静态分配 */
				static StaticQueue_t xStaticTimerQueue;
				static uint8_t ucStaticTimerQueueStorage[ configTIMER_QUEUE_LENGTH * sizeof( DaemonTaskMessage_t ) ];

				xTimerQueue = xQueueCreateStatic( ( UBaseType_t ) configTIMER_QUEUE_LENGTH, sizeof( DaemonTaskMessage_t ), &( ucStaticTimerQueueStorage[ 0 ] ), &xStaticTimerQueue );
			}
			#else
			{
				xTimerQueue = xQueueCreate( ( UBaseType_t ) configTIMER_QUEUE_LENGTH, sizeof( DaemonTaskMessage_t ) );
			}
			#endif

			#if ( configQUEUE_REGISTRY_SIZE > 0 )
			{
				if( xTimerQueue != NULL )
				{
					vQueueAddToRegistry( xTimerQueue, "TmrQ" );
				}
				else
				{
					mtCOVERAGE_TEST_MARKER();
				}
			}
			#endif /* configQUEUE_REGISTRY_SIZE */
		}
		else
		{
			mtCOVERAGE_TEST_MARKER();
		}
	}
	taskEXIT_CRITICAL();
}
/*-----------------------------------------------------------*/

BaseType_t xTimerIsTimerActive( TimerHandle_t xTimer )
{
BaseType_t xTimerIsInActiveList;
Timer_t *pxTimer = ( Timer_t * ) xTimer;

	configASSERT( xTimer );

	/* 定时器是否在活动定时器列表中? */
	taskENTER_CRITICAL();
	{
		/* 检查看定时器是否在NULL列表中，实际上是检查它是否在当前定时器列表或溢出定时器列表，
		二者之一中，但是逻辑是反的，所以要加上"!" */
		xTimerIsInActiveList = ( BaseType_t ) !( listIS_CONTAINED_WITHIN( NULL, &( pxTimer->xTimerListItem ) ) );
	}
	taskEXIT_CRITICAL();

	return xTimerIsInActiveList;
} /*lint !e818 由于typedef，不能指向const. */
/*-----------------------------------------------------------*/

void *pvTimerGetTimerID( const TimerHandle_t xTimer )
{
Timer_t * const pxTimer = ( Timer_t * ) xTimer;
void *pvReturn;

	configASSERT( xTimer );

	taskENTER_CRITICAL();
	{
		pvReturn = pxTimer->pvTimerID;
	}
	taskEXIT_CRITICAL();

	return pvReturn;
}
/*-----------------------------------------------------------*/

void vTimerSetTimerID( TimerHandle_t xTimer, void *pvNewID )
{
Timer_t * const pxTimer = ( Timer_t * ) xTimer;

	configASSERT( xTimer );

	taskENTER_CRITICAL();
	{
		pxTimer->pvTimerID = pvNewID;
	}
	taskEXIT_CRITICAL();
}
/*-----------------------------------------------------------*/

#if( INCLUDE_xTimerPendFunctionCall == 1 )

	BaseType_t xTimerPendFunctionCallFromISR( PendedFunction_t xFunctionToPend, void *pvParameter1, uint32_t ulParameter2, BaseType_t *pxHigherPriorityTaskWoken )
	{
	DaemonTaskMessage_t xMessage;
	BaseType_t xReturn;

		/* 携带函数参数，并发送到守护任务去. */
		xMessage.xMessageID = tmrCOMMAND_EXECUTE_CALLBACK_FROM_ISR;
		xMessage.u.xCallbackParameters.pxCallbackFunction = xFunctionToPend;
		xMessage.u.xCallbackParameters.pvParameter1 = pvParameter1;
		xMessage.u.xCallbackParameters.ulParameter2 = ulParameter2;

		xReturn = xQueueSendFromISR( xTimerQueue, &xMessage, pxHigherPriorityTaskWoken );

		tracePEND_FUNC_CALL_FROM_ISR( xFunctionToPend, pvParameter1, ulParameter2, xReturn );

		return xReturn;
	}

#endif /* INCLUDE_xTimerPendFunctionCall */
/*-----------------------------------------------------------*/

#if( INCLUDE_xTimerPendFunctionCall == 1 )

	BaseType_t xTimerPendFunctionCall( PendedFunction_t xFunctionToPend, void *pvParameter1, uint32_t ulParameter2, TickType_t xTicksToWait )
	{
	DaemonTaskMessage_t xMessage;
	BaseType_t xReturn;

		/* 该函数只能在调度器启动之后或定时器创建之后执行，因为只有定时器创建了
		或者调度器启动了之后，xTimerQueue才能使用. */
		configASSERT( xTimerQueue );

		/* 携带函数参数，并发送到守护任务去. */
		xMessage.xMessageID = tmrCOMMAND_EXECUTE_CALLBACK;
		xMessage.u.xCallbackParameters.pxCallbackFunction = xFunctionToPend;
		xMessage.u.xCallbackParameters.pvParameter1 = pvParameter1;
		xMessage.u.xCallbackParameters.ulParameter2 = ulParameter2;

		xReturn = xQueueSendToBack( xTimerQueue, &xMessage, xTicksToWait );

		tracePEND_FUNC_CALL( xFunctionToPend, pvParameter1, ulParameter2, xReturn );

		return xReturn;
	}

#endif /* INCLUDE_xTimerPendFunctionCall */
/*-----------------------------------------------------------*/

/* 整个代码将会被跳过，如果应用不配置包括定时器功能。如果你想要包括软定时器功能
那么确保在FreeRTOSConfig.h中将configUSE_TIMERS设置为1. */
#endif /* configUSE_TIMERS == 1 */



